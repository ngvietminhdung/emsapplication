package emsapp.dungvmnguyen.dhbk.emsapp.rest;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import emsapp.dungvmnguyen.dhbk.emsapp.rest.service.UserService;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * Created by WINDOWS on 9/30/2017.
 */

public class RestClient {
    public static final String API_BASE_URL = "https://gentle-taiga-21819.herokuapp.com";
    private static RestClient instance = new RestClient();

    private Retrofit retrofit;
    private OkHttpClient httpClient;
    private UserService userService;
    private String authToken;

    private RestClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();   //
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);

        OkHttpClient.Builder httpBuilder = new OkHttpClient.Builder();
        httpBuilder.addInterceptor(logging);
        Interceptor requestInterceptor = new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder();
                if (authToken != null) {                                  // Authorize by authToken
                    requestBuilder.header("Authorization", authToken);
                }
                String contentType = original.header("Content-Type");
                if (contentType == null) {
                    requestBuilder.header("Content-Type", "application/json")
                            .method(original.method(), original.body());
                }

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        };
        httpBuilder.addInterceptor(requestInterceptor);
        httpBuilder.connectTimeout(5, TimeUnit.MINUTES)
                .writeTimeout(3, TimeUnit.MINUTES)
                .readTimeout(3, TimeUnit.MINUTES);

        httpClient = httpBuilder.build();
        retrofit = new Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addConverterFactory(JacksonConverterFactory.create(createObjectMapper()))
                .client(httpClient)
                .build();
    }

    private ObjectMapper createObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        return objectMapper;
    }

    //Receive service from user
    public UserService getUserService() {
        if (userService == null) {
            userService = createService(UserService.class); // Create a service class UserService
        }
        return userService;                                 // If userService existed, just use it again
    }

    public <S> S createService(Class<S> serviceClass) {
        return retrofit.create(serviceClass);
    }

    //Current Feature Function
    public static RestClient getInstance() {
        return instance;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }
}
