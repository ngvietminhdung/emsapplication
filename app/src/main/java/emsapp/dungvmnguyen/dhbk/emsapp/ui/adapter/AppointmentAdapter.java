package emsapp.dungvmnguyen.dhbk.emsapp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import emsapp.dungvmnguyen.dhbk.emsapp.R;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.AppointmentDTO;

/**
 * Created by WINDOWS on 10/7/2017.
 */

public class AppointmentAdapter extends ArrayAdapter<AppointmentDTO> {

    public AppointmentAdapter(Context context, int resource, List<AppointmentDTO> items) {
        super(context, resource, items);
    }

    public AppointmentAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            view =  inflater.inflate(R.layout.activity_apponitment_row, null);
        }
        AppointmentDTO p = getItem(position);
        if (p != null) {
            TextView customerName = (TextView) view.findViewById(R.id.txtName);
            customerName.setText(p.getCustomerName());

            TextView destination = (TextView) view.findViewById(R.id.txtDestination);
            destination.setText(p.getDestination());

            TextView date = (TextView) view.findViewById(R.id.txtDate);
            date.setText(p.getDate());
        }
        return view;
    }
}
