package emsapp.dungvmnguyen.dhbk.emsapp.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import emsapp.dungvmnguyen.dhbk.emsapp.R;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.AppointmentDTO;
import emsapp.dungvmnguyen.dhbk.emsapp.ui.adapter.AppointmentAdapter;

public class AppointmentFragment extends Fragment {

    ListView lvAppointment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_appointment, container, false);

        lvAppointment = (ListView) view.findViewById(R.id.lvAppointment);
        ArrayList<AppointmentDTO> appointmentArray = new ArrayList<>();
        appointmentArray.add(new AppointmentDTO("John Smitt", "123 Dinh Tien Hoan Street, District 1, HCM city", "16/01/2017"));
        appointmentArray.add(new AppointmentDTO("AIT Company", "123 Dinh Tien Hoan Street, District 1, HCM city", "16/01/2017"));
        appointmentArray.add(new AppointmentDTO("KMS Tech", "123 Dinh Tien Hoan Street, District 1, HCM city", "16/01/2017"));
        appointmentArray.add(new AppointmentDTO("Dam Truong", "123 Dinh Tien Hoan Street, District 1, HCM city", "16/01/2017"));
        appointmentArray.add(new AppointmentDTO("Anh Vu", "123 Dinh Tien Hoan Street, District 1, HCM city", "16/01/2017"));
        appointmentArray.add(new AppointmentDTO("John Smitt", "123 Dinh Tien Hoan Street, District 1, HCM city", "16/01/2017"));
        appointmentArray.add(new AppointmentDTO("AIT Company", "123 Dinh Tien Hoan Street, District 1, HCM city", "16/01/2017"));
        appointmentArray.add(new AppointmentDTO("KMS Tech", "123 Dinh Tien Hoan Street, District 1, HCM city", "16/01/2017"));
        appointmentArray.add(new AppointmentDTO("Dam Truong", "123 Dinh Tien Hoan Street, District 1, HCM city", "16/01/2017"));
        appointmentArray.add(new AppointmentDTO("Anh Vu", "123 Dinh Tien Hoan Street, District 1, HCM city", "16/01/2017"));

        AppointmentAdapter adapter = new AppointmentAdapter(
                getContext(),
                R.layout.activity_apponitment_row,
                appointmentArray
        );

        lvAppointment.setAdapter(adapter);

        lvAppointment.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showFragment();
            }
        });

        return view;
    }

    private void showFragment() {
        Fragment newFragment = new ActivationFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
