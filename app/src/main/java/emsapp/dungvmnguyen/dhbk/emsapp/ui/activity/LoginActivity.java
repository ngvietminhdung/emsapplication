package emsapp.dungvmnguyen.dhbk.emsapp.ui.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;

import emsapp.dungvmnguyen.dhbk.emsapp.R;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.RestClient;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.UserDTO;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    EditText username;
    EditText password;
    Button btnLogin;
    TextView txtForgotPass;
    Context context;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.activity_login);

        getControl();
        bindEvents();
    }

    private void getControl(){
        username = (EditText)findViewById(R.id.username);
        password = (EditText)findViewById(R.id.password);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        txtForgotPass = (TextView)findViewById(R.id.txtForgotPass);
    }

    private void bindEvents() {
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean isValid = true;
                if (StringUtils.isEmpty(username.getText().toString().trim())) {
                    isValid = false;
                }
                if (StringUtils.isEmpty(password.getText().toString().trim())) {
                    isValid = false;
                }
                if (isValid) {
                    progressDialog = new ProgressDialog(context);
                    progressDialog.setTitle("Loading");
                    progressDialog.setMessage( "Wait while loading...");
                    progressDialog.setCancelable(false);
                    progressDialog.show();

                    Call<HashMap> loginCall = RestClient.getInstance().getUserService().personLogin(new UserDTO(username.getText().toString(), password.getText().toString()));
                    loginCall.enqueue(new Callback<HashMap>() {
                        @Override
                        public void onResponse(Call<HashMap> call, Response<HashMap> response) {
                            HashMap tokenMap = response.body();
                            String firstName= (String) tokenMap.get("firstName");

                            Intent myIntent = new Intent(LoginActivity.this, AppointmentActivity.class);
                            LoginActivity.this.startActivity(myIntent);

                            progressDialog.dismiss();
                        }

                        @Override
                        public void onFailure(Call<HashMap> call, Throwable t) {
                            Toast.makeText(context, "Login failed", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }
                    });
                }
                else {
                    Toast.makeText(context, "Input fail", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
