package emsapp.dungvmnguyen.dhbk.emsapp.core;

import android.app.Application;

/**
 * Created by WINDOWS on 9/30/2017.
 */

public class Global extends Application {

    private static String token;
    private static String userName;
    private static String email;
    private static Integer bestScore;

    public static void setPrincipal(String token) {
        Global.token = token;
    }

    public static void setUserName(String userName) {
        Global.userName = userName;
    }

    public static void setEmail(String email) {
        Global.email = email;
    }

    public static void setBestScore(Integer bestScore) {
        Global.bestScore = bestScore;
    }
}
