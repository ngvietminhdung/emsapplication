package emsapp.dungvmnguyen.dhbk.emsapp.rest.dto;

/**
 * Created by WINDOWS on 10/7/2017.
 */

public class AppointmentDTO {

    private String customerName;
    private String destination;
    private String date;

    public AppointmentDTO(String customerName, String destination, String date) {
        this.customerName = customerName;
        this.destination = destination;
        this.date = date;
    }

    public String getCustomerName() {
        return customerName;
    }

    public String getDestination() {
        return destination;
    }

    public String getDate() {
        return date;
    }
}
