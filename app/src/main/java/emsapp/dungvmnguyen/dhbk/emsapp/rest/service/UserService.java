package emsapp.dungvmnguyen.dhbk.emsapp.rest.service;

import java.util.HashMap;

import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.UserDTO;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

;

public interface UserService {
    @POST("/people/person/login")
    Call<HashMap> personLogin(@Body UserDTO userDTO);
}
