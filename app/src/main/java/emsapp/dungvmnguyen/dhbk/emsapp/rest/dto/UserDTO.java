package emsapp.dungvmnguyen.dhbk.emsapp.rest.dto;

public class UserDTO {
    private  String firstname;
    private  String lastname;

    public UserDTO(String firstname, String lastname) {
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
}
